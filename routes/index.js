const express = require('express');
const { google } = require('googleapis');
const config = require('../config');
const fs = require('fs');
const moment = require('moment');

const router = express.Router();
const customsearch = google.customsearch('v1');

router.get('/search', (req, res, next) => {
  const {
    q,
    num,
    lr
  } = req.query;

  if (num > 10) return res.status(403).send({message: 'O limite de busca é de 10 resultados no total.'})

  customsearch.cse.list({
    auth: config.ggApiKey,
    cx: config.ggCx,
    q,
    lr,
    num
  }).then(result => result.data)
    .then((result) => {
      const {
        queries,
        items,
        searchInformation
      } = result;

      const data = {
        q,
        time: searchInformation.searchTime,
        items: items.map(o => ({
          title: o.title,
          link: o.link
        }))
      }
      console.table(data.items)

      const filename = `${moment().format('LL - h-mm-ss a')}`;

      fs.writeFile(`results/${filename}.json`, JSON.stringify(data, null, 2), function(err) {
        if (err) {
          console.log(err);
        }
      });
      res.status(200).send(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send(data);
    });
})

module.exports = router;
